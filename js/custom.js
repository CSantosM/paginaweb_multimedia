$(document).ready(function () {

    $('.content').hide();
    $('.content').fadeIn(400);

    $('.postal').click(function () {
        $(this).toggleClass('selected');
    })

    $('.zoom').hover(function () {
        $(this).elevateZoom({
            zoomType: "lens",
            lensSize: 200
        });
    });


    var audioElement = document.createElement('audio');
    audioElement.setAttribute('src', 'jr.mp3');
    //audioElement.setAttribute('autoplay', 'autoplay');

    $.get();

    $('#audio').click(function () {

        audioElement.addEventListener("load", function () {
            audioElement.play();
        }, true);

        if ($(this).hasClass('fi-play-circle')) {
            audioElement.play();
            $(this).removeClass('fi-play-circle');
            $(this).addClass('fi-pause');
        } else {
            audioElement.pause();
            $(this).addClass('fi-play-circle');
            $(this).removeClass('fi-pause');
        }




    });






});
$('#menuBoton').attr('data-toggle', '');
$('#menu').attr('data-responsive-menu', 'drilldown medium-dropdown');
$('#orbit').attr('data-orbit', '');
$('#orbit').attr('data-options', 'animInFromLeft:fade-in;animInFromRight:fade-in;animOutToLeft:fade-out;animOutToRight:fade-out;');
$('.reveal').attr('data-reveal', '');
$('.close-button').attr('data-close', '');

//$('#menu').attr('data-dropdown-menu', '');
//$('#submenu').attr('data-submenu', '');