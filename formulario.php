<?php
//Incluimos la clase de PHPMailer
require_once('phpmailer/class.phpmailer.php');

$correo = new PHPMailer(); //Creamos una instancia en lugar usar mail()

//Usamos el SetFrom para decirle al script quien envia el correo
$nombre = $_POST['name'];
$correo->SetFrom("c.santosmo@alumnos.urjc.es", "Postal para ".$nombre);

//Usamos el AddReplyTo para decirle al script a quien tiene que responder el correo
$correo->AddReplyTo("c.santosmo@alumnos.urjc.es","Postal");

//Usamos el AddAddress para agregar un destinatario
$destinatario = $_POST['mail'];
$correo->AddAddress($destinatario, "Robot");

//Ponemos el asunto del mensaje
$correo->Subject = "Postal desde Monaco";

/*
 * Si deseamos enviar un correo con formato HTML utilizaremos MsgHTML:
 * $correo->MsgHTML("<strong>Mi Mensaje en HTML</strong>");
 * Si deseamos enviarlo en texto plano, haremos lo siguiente:
 * $correo->IsHTML(false);
 * $correo->Body = "Mi mensaje en Texto Plano";
 */
$mensaje = $_POST['message'];
$correo->MsgHTML($mensaje);

//Si deseamos agregar un archivo adjunto utilizamos AddAttachment
$correo->AddAttachment("img/postal/casino.jpg");

//Enviamos el correo
if(!$correo->Send()) {
  echo "Hubo un error: " . $correo->ErrorInfo;
} else {
  echo "Mensaje enviado con exito.";
}

?>